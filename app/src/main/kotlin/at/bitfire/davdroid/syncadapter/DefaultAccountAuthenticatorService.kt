/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.syncadapter

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.accounts.OnAccountsUpdateListener
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.AnyThread
import at.bitfire.davdroid.OpenIdUtils
import at.bitfire.davdroid.db.AppDatabase
import at.bitfire.davdroid.log.Logger
import at.bitfire.davdroid.resource.LocalAddressBook
import at.bitfire.davdroid.settings.AccountSettings
import at.bitfire.davdroid.ui.setup.LoginActivity
import at.bitfire.davdroid.util.AuthStatePrefUtils
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.openid.appauth.AuthState
import net.openid.appauth.AuthorizationService
import java.util.logging.Level

abstract class DefaultAccountAuthenticatorService : Service(), OnAccountsUpdateListener {

    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface DefaultAccountAuthenticatorServiceEntryPoint {
        fun appDatabase(): AppDatabase
    }

    companion object {
        fun cleanupAccounts(context: Context, db: AppDatabase) {
            Logger.log.info("Cleaning up orphaned accounts")

            val accountNames = HashSet<String>()
            val accounts = AccountUtils.getMainAccounts(context)

            for (account in accounts.toTypedArray()) {
                accountNames += account.name
            }

            // delete orphaned address book accounts
            val addressBookAccounts = AccountUtils.getAddressBookAccounts(context)
            addressBookAccounts.map { LocalAddressBook(context, it, null) }
                .forEach {
                    try {
                        if (!accountNames.contains(it.mainAccount.name)) {
                            it.delete()
                        }
                    } catch (e: Exception) {
                        Logger.log.log(Level.SEVERE, "Couldn't delete address book account", e)
                    }
                }

            // delete orphaned services in DB
            val serviceDao = db.serviceDao()
            if (accountNames.isEmpty()) {
                serviceDao.deleteAll()
            } else {
                serviceDao.deleteExceptAccounts(accountNames.toTypedArray())
            }
        }

    }

    private lateinit var accountManager: AccountManager
    private lateinit var accountAuthenticator: AccountAuthenticator

    override fun onCreate() {
        accountManager = AccountManager.get(this)
        accountManager.addOnAccountsUpdatedListener(this, null, true)

        accountAuthenticator = AccountAuthenticator(this)
    }

    override fun onDestroy() {
        accountManager.removeOnAccountsUpdatedListener(this)
        super.onDestroy()
    }

    override fun onBind(intent: Intent?) =
        accountAuthenticator.iBinder.takeIf { intent?.action == AccountManager.ACTION_AUTHENTICATOR_INTENT }

    @AnyThread
    override fun onAccountsUpdated(accounts: Array<out Account>?) {
        CoroutineScope(Dispatchers.IO).launch {
            val db = EntryPointAccessors.fromApplication(
                applicationContext,
                DefaultAccountAuthenticatorServiceEntryPoint::class.java
            ).appDatabase()

            cleanupAccounts(applicationContext, db)
        }
    }

    private class AccountAuthenticator(
        val context: Context
    ) : AbstractAccountAuthenticator(context) {

        override fun addAccount(
            response: AccountAuthenticatorResponse?,
            accountType: String?,
            authTokenType: String?,
            requiredFeatures: Array<String>?,
            options: Bundle?
        ): Bundle {
            val intent = Intent(context, LoginActivity::class.java)
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
            intent.putExtra(
                LoginActivity.ACCOUNT_TYPE,
                accountType
            )

            options?.let {
                intent.putExtra(
                    LoginActivity.OPEN_APP_PACKAGE_AFTER_AUTH,
                    it.getString(LoginActivity.OPEN_APP_PACKAGE_AFTER_AUTH)
                )
                intent.putExtra(
                    LoginActivity.OPEN_APP_ACTIVITY_AFTER_AUTH,
                    it.getString(LoginActivity.OPEN_APP_ACTIVITY_AFTER_AUTH)
                )

                intent.putExtra(
                    LoginActivity.USERNAME_HINT,
                    it.getString(LoginActivity.USERNAME_HINT)
                )
            }

            val bundle = Bundle(1)
            bundle.putParcelable(AccountManager.KEY_INTENT, intent)
            return bundle
        }

        override fun editProperties(response: AccountAuthenticatorResponse?, accountType: String?) =
            null

        override fun getAuthTokenLabel(p0: String?) = null

        override fun confirmCredentials(
            rssponse: AccountAuthenticatorResponse?,
            account: Account?,
            options: Bundle?
        ) = null

        override fun updateCredentials(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            authTokenType: String?,
            options: Bundle?
        ) = null

        override fun getAuthToken(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            authTokenType: String?,
            options: Bundle?
        ): Bundle? {
            val accountManager = AccountManager.get(context)
            val authStateString = accountManager.getUserData(account, AccountSettings.KEY_AUTH_STATE) ?: return null

            val authState = AuthState.jsonDeserialize(authStateString)

            if (authState == null) {
                val result = Bundle()
                result.putInt(
                    AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
                    AccountManager.ERROR_CODE_UNSUPPORTED_OPERATION
                )
                return result
            }

            if (!authState.needsTokenRefresh) {
                val result = Bundle()
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account!!.name)
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
                result.putString(AccountManager.KEY_AUTHTOKEN, authState.accessToken)
                return result
            }

            val tokenRequest = authState.createTokenRefreshRequest()
            val clientSecretString = accountManager.getUserData(account, AccountSettings.KEY_CLIENT_SECRET)
            val clientSecret = OpenIdUtils.getClientAuthentication(clientSecretString)

            val authorizationService = AuthorizationService(context)

            authorizationService.performTokenRequest(
                tokenRequest,
                clientSecret
            ) { tokenResponse, ex ->
                authState.update(tokenResponse, ex)
                accountManager.setUserData(
                    account,
                    AccountSettings.KEY_AUTH_STATE,
                    authState.jsonSerializeString()
                )
                accountManager.setUserData(
                    account,
                    AccountSettings.KEY_CLIENT_SECRET,
                    clientSecretString
                )

                AuthStatePrefUtils.saveAuthState(context, account!!, authState.jsonSerializeString())

                val result = Bundle()
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name)
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
                result.putString(AccountManager.KEY_AUTHTOKEN, authState.accessToken)
                response?.onResult(result)

                authorizationService.dispose()
            }

            val result = Bundle()
            result.putInt(
                AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
                AccountManager.ERROR_CODE_UNSUPPORTED_OPERATION
            )
            return result
        }

        override fun hasFeatures(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            features: Array<out String>?
        ) = null
    }
}
