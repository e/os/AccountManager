/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.ui.setup

import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import at.bitfire.davdroid.ECloudAccountHelper

class EeloAuthenticatorModel(application: Application) : AndroidViewModel(application) {

    companion object {
        // as https://gitlab.e.foundation/e/backlog/-/issues/6287 is blocked, the openId implementation is not ready yet.
        // But we want to push the changes so later we won't face any conflict. So we are disabling the openId feature for now.
        const val enableOpenIdSupport = false
    }

    private var initialized = false


    val loginWithUrlAndTokens = MutableLiveData<Boolean>()

    val baseUrl = MutableLiveData<String>()
    val baseUrlError = MutableLiveData<String>()

    val username = MutableLiveData<String>()

    val password = MutableLiveData<String>()

    val certificateAlias = MutableLiveData<String>()

    init {
        loginWithUrlAndTokens.value = true
    }

    fun initialize(intent: Intent) {
        if (initialized)
            return

        // we've got initial login data
        val givenUrl = intent.getStringExtra(LoginActivity.EXTRA_URL)
        val givenPassword = intent.getStringExtra(LoginActivity.EXTRA_PASSWORD)

        baseUrl.value = givenUrl

        password.value = givenPassword

        initialized = true
    }

    fun blockProceedWithLogin(): Boolean {
        val context = getApplication<Application>()
        return ECloudAccountHelper.alreadyHasECloudAccount(context)
    }
}
