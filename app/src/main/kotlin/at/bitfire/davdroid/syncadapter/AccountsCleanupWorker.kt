/***************************************************************************************************
 * Copyright © All Contributors. See LICENSE and AUTHORS in the root directory for details.
 **************************************************************************************************/

package at.bitfire.davdroid.syncadapter

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import at.bitfire.davdroid.db.AppDatabase
import at.bitfire.davdroid.log.Logger
import at.bitfire.davdroid.resource.LocalAddressBook
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit
import java.util.logging.Level

@HiltWorker
class AccountsCleanupWorker @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParameters: WorkerParameters,
    val db: AppDatabase
): Worker(appContext, workerParameters) {

    companion object {
        const val NAME = "accounts-cleanup"

        private val mutex = Semaphore(1)
        /**
         * Prevents account cleanup from being run until `unlockAccountsCleanup` is called.
         * Can only be active once at the same time globally (blocking).
         */
        fun lockAccountsCleanup() = mutex.acquire()
        /** Must be called exactly one time after calling `lockAccountsCleanup`. */
        fun unlockAccountsCleanup() = mutex.release()

        fun enqueue(context: Context) {
            WorkManager.getInstance(context).enqueueUniqueWork(NAME, ExistingWorkPolicy.KEEP,
                OneTimeWorkRequestBuilder<AccountsCleanupWorker>()
                    .setInitialDelay(15, TimeUnit.SECONDS)   // wait some time before cleaning up accouts
                    .build())
        }
    }

    override fun doWork(): Result {
        lockAccountsCleanup()
        try {
            cleanupAccounts(applicationContext)
        } finally {
            unlockAccountsCleanup()
        }
        return Result.success()
    }

    private fun cleanupAccounts(context: Context) {
        Logger.log.log(Level.INFO, "Cleaning up accounts. Current accounts")

        val mainAccountNames = HashSet<String>()
        val accountFromManager = AccountUtils.getMainAccounts(context)

        for (account in accountFromManager.toTypedArray()) {
            mainAccountNames += account.name
        }

        // delete orphaned address book accounts
        val addressBookAccounts = AccountUtils.getAddressBookAccounts(context)
        addressBookAccounts.map { LocalAddressBook(context, it, null) }
            .forEach {
                try {
                    if (!mainAccountNames.contains(it.mainAccount.name))
                        it.delete()
                } catch(e: Exception) {
                    Logger.log.log(Level.SEVERE, "Couldn't delete address book account", e)
                }
            }

        // delete orphaned services in DB
        val serviceDao = db.serviceDao()
        if (mainAccountNames.isEmpty())
            serviceDao.deleteAll()
        else
            serviceDao.deleteExceptAccounts(mainAccountNames.toTypedArray())
    }

}