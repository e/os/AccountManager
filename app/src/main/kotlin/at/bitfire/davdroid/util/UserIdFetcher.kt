/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.util

object UserIdFetcher {

    /**
     * retrieve the userId from caldav/carddav nextcloud principal url.
     * example: if the url is: https://abc.com/remote.php/dav/principals/users/xyz/, then this function will return xyz.
     *
     * this function will return null, if
     * - `/users/` part is missing
     */
    fun fetch(principalUrl: String): String? {
        val usersPart = "/users/"

        var userId: String? = null
        if (principalUrl.contains(usersPart, ignoreCase = true)) {
            userId = principalUrl.split(usersPart, ignoreCase = true)[1]
            if (userId.endsWith("/")) {
                userId = userId.dropLast(1)
            }

            if (userId.isBlank()) {
                userId = null
            }
        }

        return userId
    }
}
