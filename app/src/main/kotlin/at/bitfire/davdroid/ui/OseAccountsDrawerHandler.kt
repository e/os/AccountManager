/***************************************************************************************************
 * Copyright © All Contributors. See LICENSE and AUTHORS in the root directory for details.
 **************************************************************************************************/

package at.bitfire.davdroid.ui

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.view.MenuItem
import at.bitfire.davdroid.R
import at.bitfire.davdroid.log.Logger
import at.bitfire.davdroid.ui.webdav.WebdavMountsActivity
import java.util.logging.Level
import javax.inject.Inject

/**
 * Default menu items control
 */
class OseAccountsDrawerHandler @Inject constructor(): BaseAccountsDrawerHandler() {

    companion object {
        private const val WEB_CAL_MANAGER_PACKAGE = "foundation.e.webcalendarmanager"
        private const val WEB_CAL_MANAGER_MAIN_ACTIVITY = "at.bitfire.icsdroid.ui.views.CalendarListActivity"
    }

    override fun onNavigationItemSelected(activity: Activity, item: MenuItem) {
        when (item.itemId) {
            R.id.nav_webdav_mounts ->
                activity.startActivity(Intent(activity, WebdavMountsActivity::class.java))
            R.id.nav_open_webcalmanager ->
                openWebCalManager(activity)
            else ->
                super.onNavigationItemSelected(activity, item)
        }
    }

    private fun openWebCalManager(activity: Activity) {
        try {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.component =
                ComponentName(WEB_CAL_MANAGER_PACKAGE, WEB_CAL_MANAGER_MAIN_ACTIVITY)
            activity.startActivity(intent)
        } catch (e: Exception) {
            Logger.log.log(Level.SEVERE, "Failed to open WebCalendarManager", e)
        }
    }

}