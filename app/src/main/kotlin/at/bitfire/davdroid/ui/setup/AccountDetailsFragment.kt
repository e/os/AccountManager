/***************************************************************************************************
 * Copyright © All Contributors. See LICENSE and AUTHORS in the root directory for details.
 **************************************************************************************************/

package at.bitfire.davdroid.ui.setup

import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.app.Activity
import android.app.Application
import android.content.ComponentName
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.CalendarContract
import android.text.Editable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import at.bitfire.davdroid.BuildConfig
import at.bitfire.davdroid.Constants
import at.bitfire.davdroid.InvalidAccountException
import at.bitfire.davdroid.R
import at.bitfire.davdroid.authorization.IdentityProvider
import at.bitfire.davdroid.databinding.LoginAccountDetailsBinding
import at.bitfire.davdroid.db.AppDatabase
import at.bitfire.davdroid.db.Credentials
import at.bitfire.davdroid.db.HomeSet
import at.bitfire.davdroid.db.Service
import at.bitfire.davdroid.log.Logger
import at.bitfire.davdroid.resource.TaskUtils
import at.bitfire.davdroid.servicedetection.DavResourceFinder
import at.bitfire.davdroid.servicedetection.RefreshCollectionsWorker
import at.bitfire.davdroid.settings.AccountSettings
import at.bitfire.davdroid.settings.SettingsManager
import at.bitfire.davdroid.syncadapter.AccountUtils
import at.bitfire.davdroid.syncadapter.SyncAllAccountWorker
import at.bitfire.davdroid.syncadapter.SyncWorker
import at.bitfire.davdroid.util.AuthStatePrefUtils
import at.bitfire.vcard4android.GroupMethod
import com.google.android.material.snackbar.Snackbar
import com.nextcloud.android.utils.AccountManagerUtils
import com.owncloud.android.lib.common.accounts.AccountTypeUtils
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.launch
import net.openid.appauth.AuthorizationService
import net.openid.appauth.EndSessionRequest
import java.util.logging.Level
import javax.inject.Inject

@AndroidEntryPoint
class AccountDetailsFragment : Fragment() {

    @Inject lateinit var settings: SettingsManager

    val loginModel by activityViewModels<LoginModel>()
    val model by viewModels<Model>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = LoginAccountDetailsBinding.inflate(inflater, container, false)
        v.lifecycleOwner = viewLifecycleOwner
        v.details = model

        val config = loginModel.configuration ?: throw IllegalStateException()

        // default account name
        model.name.value =
                loginModel.credentials?.userName
                        ?: config.calDAV?.emails?.firstOrNull()
                        ?: loginModel.suggestedAccountName
                        ?: loginModel.credentials?.certificateAlias
                        ?: loginModel.baseURI?.host

        // CardDAV-specific
        v.carddav.visibility = if (config.cardDAV != null) View.VISIBLE else View.GONE
        if (settings.containsKey(AccountSettings.KEY_CONTACT_GROUP_METHOD))
            v.contactGroupMethod.isEnabled = false

        v.contactGroupMethod.setSelection(1)

        // CalDAV-specific
        config.calDAV?.let {
            val accountNameAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_list_item_1, it.emails)
            v.accountName.setAdapter(accountNameAdapter)
        }

        v.createAccount.setOnClickListener {
            val name = model.name.value
            if (name.isNullOrBlank())
                model.nameError.value = getString(R.string.login_account_name_required)
            else {
                // check whether account name already exists
                if (AccountUtils.getMainAccounts(requireContext()).any { it.name == name }) {
                    model.nameError.value = getString(R.string.login_account_name_already_taken)
                    return@setOnClickListener
                }

                val idx = v.contactGroupMethod.selectedItemPosition
                val groupMethodName = resources.getStringArray(R.array.settings_contact_group_method_values)[idx]

                v.createAccountProgress.visibility = View.VISIBLE
                v.createAccount.visibility = View.GONE

                model.createAccount(
                    requireActivity(),
                    name,
                    loginModel.credentials,
                    config,
                    GroupMethod.valueOf(groupMethodName)
                ).observe(viewLifecycleOwner, Observer { success ->
                    if (success) {
                        // close Create account activity
                        requireActivity().finish()
                    } else {
                        Snackbar.make(requireActivity().findViewById(android.R.id.content), R.string.login_account_not_created, Snackbar.LENGTH_LONG).show()

                        v.createAccountProgress.visibility = View.GONE
                        v.createAccount.visibility = View.VISIBLE
                    }
                })
            }
        }

        val forcedGroupMethod = settings.getString(AccountSettings.KEY_CONTACT_GROUP_METHOD)?.let { GroupMethod.valueOf(it) }
        if (forcedGroupMethod != null) {
            // contact group type forced by settings
            v.contactGroupMethod.isEnabled = false
            for ((i, method) in resources.getStringArray(R.array.settings_contact_group_method_values).withIndex()) {
                if (method == forcedGroupMethod.name) {
                    v.contactGroupMethod.setSelection(i)
                    break
                }
            }
        } else {
            // contact group type selectable
            v.contactGroupMethod.isEnabled = true
            for ((i, method) in resources.getStringArray(R.array.settings_contact_group_method_values).withIndex()) {
                // take suggestion from detection process into account
                if (method == loginModel.suggestedGroupMethod.name) {
                    v.contactGroupMethod.setSelection(i)
                    break
                }
            }
        }

        val providedAccountType = requireActivity().intent.getStringExtra(LoginActivity.ACCOUNT_TYPE)
        if ((providedAccountType != getString(R.string.account_type)) && (providedAccountType in AccountUtils.getMainAccountTypes(requireContext()))) {

            v.mainDetailLayout.visibility = View.GONE
            v.mainLoadingLayout.visibility = View.VISIBLE

            val name = model.name.value
            if (name.isNullOrBlank())
                model.nameError.value = getString(R.string.login_account_name_required)
            else {
                val idx = v.contactGroupMethod.selectedItemPosition
                val groupMethodName = resources.getStringArray(R.array.settings_contact_group_method_values)[idx]

                model.createAccount(
                    requireActivity(),
                    name,
                    loginModel.credentials!!,
                    config,
                    GroupMethod.valueOf(groupMethodName)
                ).observe(viewLifecycleOwner, Observer { success ->
                    if (success) {
                        Toast.makeText(context, R.string.message_account_added_successfully, Toast.LENGTH_LONG).show()
                        requireActivity().setResult(Activity.RESULT_OK)
                        requireActivity().finish()

                        if (requireActivity().intent.hasExtra(AccountManager
                                .KEY_ACCOUNT_AUTHENTICATOR_RESPONSE) && requireActivity().intent
                                .getParcelableExtra<AccountAuthenticatorResponse>(AccountManager
                                    .KEY_ACCOUNT_AUTHENTICATOR_RESPONSE) != null) {
                            requireActivity().intent
                                .getParcelableExtra<AccountAuthenticatorResponse>(AccountManager
                                    .KEY_ACCOUNT_AUTHENTICATOR_RESPONSE)?.onResult(null)
                        }

                        if (requireActivity().intent.getStringExtra(LoginActivity.ACCOUNT_TYPE) == getString(R.string.eelo_account_type)) {
                            notifyEdrive(name)
                        }

                        handlePostAuthOperations()
                    }
                })
            }
        }

        return v.root
    }

    private fun notifyEdrive(name: String) {
        val intent = Intent("foundation.e.drive.action.ADD_ACCOUNT")
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
        intent.component =
            ComponentName(
                getString(R.string.e_drive_package_name), 
                "foundation.e.drive.account.receivers.AccountAddedReceiver"
            )

        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, name)
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, getString(R.string.eelo_account_type))
        requireActivity().sendBroadcast(intent)
    }


    private fun handlePostAuthOperations() {
        val packageToBeOpened = requireActivity().intent.getStringExtra(LoginActivity.OPEN_APP_PACKAGE_AFTER_AUTH)
        val activityToBeOpened = requireActivity().intent.getStringExtra(LoginActivity.OPEN_APP_ACTIVITY_AFTER_AUTH)
        openApp(packageToBeOpened, activityToBeOpened)
    }

    private fun openApp(packageToBeOpened: String?, activityToBeOpened: String?) {
        if (!TextUtils.isEmpty(packageToBeOpened) && !TextUtils.isEmpty(activityToBeOpened)) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.putExtra(LoginActivity.IGNORE_ACCOUNT_SETUP, true)
            // ignore account setup on open, because sync adapter will handle it
            // intent.putExtra(LoginActivity.IGNORE_ACCOUNT_SETUP, true) 
            intent.component = ComponentName(packageToBeOpened!!, activityToBeOpened!!)
            requireActivity().applicationContext.startActivity(intent)
        }
    }

    @HiltViewModel
    class Model @Inject constructor(
        application: Application,
        val db: AppDatabase,
        val settingsManager: SettingsManager
    ) : AndroidViewModel(application) {

        val name = MutableLiveData<String>()
        val nameError = MutableLiveData<String>()
        val showApostropheWarning = MutableLiveData<Boolean>(false)

        val context: Context get() = getApplication()

        fun validateAccountName(s: Editable) {
            showApostropheWarning.value = s.toString().contains('\'')
            nameError.value = null
        }

        /**
         * Creates a new main account with discovered services and enables periodic syncs with
         * default sync interval times.
         *
         * @param name Name of the account
         * @param credentials Server credentials
         * @param config Discovered server capabilities for syncable authorities
         * @param groupMethod Whether CardDAV contact groups are separate VCards or as contact categories
         * @return *true* if account creation was succesful; *false* otherwise (for instance because an account with this name already exists)
         */
        fun createAccount(activity: Activity, name: String, credentials: Credentials?, config: DavResourceFinder.Configuration, groupMethod: GroupMethod): LiveData<Boolean> {
            val result = MutableLiveData<Boolean>()
            viewModelScope.launch(Dispatchers.Default + NonCancellable) {
                var accountType = context.getString(R.string.account_type)
                var addressBookAccountType = context.getString(R.string.account_type_address_book)

                var baseURL : String? = null
                if (config.calDAV != null) {
                    baseURL = config.calDAV.principal?.toString()
                }

                if (baseURL == null) {
                    baseURL = config.cardDAV?.principal?.toString()
                }

                when (activity.intent.getStringExtra(LoginActivity.ACCOUNT_TYPE)) {
                    context.getString(R.string.eelo_account_type) -> {
                        accountType = context.getString(R.string.eelo_account_type)
                        addressBookAccountType = context.getString(R.string.account_type_eelo_address_book)
                    }
                    context.getString(R.string.google_account_type) -> {
                        accountType = context.getString(R.string.google_account_type)
                        addressBookAccountType = context.getString(R.string.account_type_google_address_book)
                        baseURL = null
                    }
                    context.getString(R.string.yahoo_account_type) -> {
                        accountType = context.getString(R.string.yahoo_account_type)
                        addressBookAccountType = context.getString(R.string.account_type_yahoo_address_book)
                        baseURL = null
                    }
                }

                val account = Account(name, accountType)

                // create Android account
                val userData = AccountSettings.initialUserData(credentials, baseURL, config.cookies, config.calDAV?.emails?.firstOrNull())

                AuthStatePrefUtils.saveAuthState(context, account, credentials?.authState?.jsonSerializeString())

                Logger.log.log(Level.INFO, "Creating Android account with initial config", arrayOf(account, userData))

                val accountManager = AccountManager.get(context)

                if (!AccountUtils.createAccount(context, account, userData, credentials?.password)) {
                    if (accountType in AccountUtils.getOpenIdMainAccountTypes(context) && credentials?.authState != null) {
                        updateAuthState(userData, accountManager, account)
                    } else {
                        result.postValue(false)
                        return@launch
                    }
                }

                if (!credentials?.authState?.accessToken.isNullOrEmpty()) {
                    accountManager.setAuthToken(account, Constants.AUTH_TOKEN_TYPE, credentials?.authState?.accessToken)
                }

                var pass: String? = null

                if (!credentials?.password.isNullOrEmpty()) {
                    pass = credentials?.password
                }

                pass?.let {
                    if (accountType == AccountManagerUtils.getAccountType(context)) {
                        accountManager.setAuthToken(account, AccountTypeUtils.getAuthTokenTypePass(account.type), it)
                        return@let
                    }

                    accountManager.setPassword(account, credentials?.password)
                }


                ContentResolver.setSyncAutomatically(account, context.getString(R.string.notes_authority), true)
                ContentResolver.setSyncAutomatically(account, context.getString(R.string.email_authority), true)
                ContentResolver.setSyncAutomatically(account, context.getString(R.string.media_authority), true)
                ContentResolver.setSyncAutomatically(account, context.getString(R.string.app_data_authority), true)
                ContentResolver.setSyncAutomatically(account, context.getString(R.string.metered_edrive_authority), true)

                // add entries for account to service DB
                Logger.log.log(Level.INFO, "Writing account configuration to database", config)
                try {
                    val accountSettings = AccountSettings(context, account)
                    val defaultSyncInterval = Constants.DEFAULT_CALENDAR_SYNC_INTERVAL

                    // Configure CardDAV service
                    val addrBookAuthority = context.getString(R.string.address_books_authority)
                    if (config.cardDAV != null) {
                        // insert CardDAV service
                        val id = insertService(
                            credentials?.userName ?: "",
                            credentials?.authState?.jsonSerializeString(),
                            accountType,
                            addressBookAccountType,
                            Service.TYPE_CARDDAV,
                            config.cardDAV
                        )

                        // initial CardDAV account settings
                        accountSettings.setGroupMethod(groupMethod)

                        // start CardDAV service detection (refresh collections)
                        RefreshCollectionsWorker.refreshCollections(context, id)

                        // set default sync interval and enable sync regardless of permissions
                        ContentResolver.setIsSyncable(account, addrBookAuthority, 1)
                        accountSettings.setSyncInterval(addrBookAuthority, defaultSyncInterval)
                    } else
                        ContentResolver.setIsSyncable(account, addrBookAuthority, 0)

                    // Configure CalDAV service
                    if (config.calDAV != null) {
                        // insert CalDAV service
                        val id = insertService(
                            credentials?.userName ?: "",
                            credentials?.authState?.jsonSerializeString(),
                            accountType,
                            addressBookAccountType,
                            Service.TYPE_CALDAV,
                            config.calDAV
                        )

                        // start CalDAV service detection (refresh collections)
                        RefreshCollectionsWorker.refreshCollections(context, id)

                        // set default sync interval and enable sync regardless of permissions
                        ContentResolver.setIsSyncable(account, CalendarContract.AUTHORITY, 1)
                        accountSettings.setSyncInterval(CalendarContract.AUTHORITY, defaultSyncInterval)

                        // if task provider present, set task sync interval and enable sync
                        val taskProvider = TaskUtils.currentProvider(context)
                        if (taskProvider != null) {
                            ContentResolver.setIsSyncable(account, taskProvider.authority, 1)
                            accountSettings.setSyncInterval(taskProvider.authority, defaultSyncInterval)
                            // further changes will be handled by TasksWatcher on app start or when tasks app is (un)installed
                            Logger.log.info("Tasks provider ${taskProvider.authority} found. Tasks sync enabled.")
                        } else
                            Logger.log.info("No tasks provider found. Did not enable tasks sync.")
                    } else
                        ContentResolver.setIsSyncable(account, CalendarContract.AUTHORITY, 0)

                } catch(e: InvalidAccountException) {
                    Logger.log.log(Level.SEVERE, "Couldn't access account settings", e)
                    result.postValue(false)
                    return@launch
                }

                SyncWorker.enqueueAllAuthorities(activity, account)
                SyncAllAccountWorker.enqueue(context, 2)

                result.postValue(true)
            }
            return result
        }

        private fun updateAuthState(
            userData: Bundle,
            accountManager: AccountManager,
            account: Account
        ) {
            for (openIdAccount in AccountUtils.getOpenIdMainAccounts(context)) {
                if (userData.get(AccountSettings.KEY_EMAIL_ADDRESS) == accountManager
                        .getUserData(account, AccountSettings.KEY_EMAIL_ADDRESS)
                ) {
                    val authState = userData.getString(AccountSettings.KEY_AUTH_STATE)

                    accountManager.setUserData(
                        openIdAccount, AccountSettings.KEY_AUTH_STATE,
                        authState
                    )
                    accountManager.setUserData(
                        openIdAccount, AccountSettings.KEY_CLIENT_SECRET,
                        userData.getString(AccountSettings.KEY_CLIENT_SECRET)
                    )

                    AuthStatePrefUtils.saveAuthState(context, openIdAccount, authState)
                }
            }
        }

        private fun insertService(
            accountName: String,
            authState: String?,
            accountType: String,
            addressBookAccountType: String,
            type: String,
            info: DavResourceFinder.Configuration.ServiceInfo
        ): Long {
            // insert service
            val service = Service(0, accountName, authState, accountType, addressBookAccountType, type, info.principal)
            val serviceId = db.serviceDao().insertOrReplace(service)

            // insert home sets
            val homeSetDao = db.homeSetDao()
            for (homeSet in info.homeSets)
                homeSetDao.insertOrUpdateByUrl(HomeSet(0, serviceId, true, homeSet))

            // insert collections
            val collectionDao = db.collectionDao()
            for (collection in info.collections.values) {
                collection.serviceId = serviceId
                collectionDao.insertOrUpdateByUrl(collection)
            }

            return serviceId
        }
    }
}
