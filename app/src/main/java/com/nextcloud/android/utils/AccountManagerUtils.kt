package com.nextcloud.android.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent

object WebViewUtils {
    fun openCustomTab(context: Context, url: String) {
        val packageManager = context.packageManager
        val resolveInfo = packageManager.queryIntentActivities(
            Intent(Intent.ACTION_VIEW, Uri.parse(url)),
            PackageManager.MATCH_DEFAULT_ONLY
        )

        if (resolveInfo.isNotEmpty()) {
            val customTabsIntent = CustomTabsIntent.Builder()
                .setShowTitle(true).build()
            customTabsIntent.intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            customTabsIntent.launchUrl(context, Uri.parse(url))
        } else {
            // Fallback to default browser
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(intent)
        }
    }
}