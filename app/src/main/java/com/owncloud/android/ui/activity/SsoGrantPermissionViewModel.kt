/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.owncloud.android.ui.activity

import android.accounts.Account
import android.accounts.AccountManager
import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import at.bitfire.davdroid.R
import at.bitfire.davdroid.db.AppDatabase
import at.bitfire.davdroid.log.Logger.log
import at.bitfire.davdroid.util.UserIdFetcher
import com.nextcloud.android.sso.Constants
import com.nextcloud.android.utils.EncryptionUtils
import com.owncloud.android.lib.common.OwnCloudAccount
import com.owncloud.android.lib.common.accounts.AccountUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.util.UUID
import java.util.logging.Level
import javax.inject.Inject

@HiltViewModel
class SsoGrantPermissionViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val database: AppDatabase,
) : ViewModel() {

    private val acceptedAccountTypes = listOf(context.getString(R.string.eelo_account_type))
    private val acceptedPackages = listOf("foundation.e.notes")

    private val _permissionEvent = MutableSharedFlow<SsoGrantPermissionEvent>()
    val permissionEvent = _permissionEvent.asSharedFlow()

    fun initValidation(callingActivity: ComponentName?, account: Account?) {
        viewModelScope.launch(Dispatchers.IO) {
            val packageName = getCallingPackageName(callingActivity) ?: return@launch
            validate(packageName, account)
        }
    }

    private suspend fun emitPermissionDeniedEvent(message: String) {
        _permissionEvent.emit(
            SsoGrantPermissionEvent.PermissionDenied(
                errorMessage = message
            )
        )
    }

    private suspend fun getCallingPackageName(callingActivity: ComponentName?): String? {
        if (callingActivity != null) {
            return callingActivity.packageName
        }

        log.log(Level.SEVERE, "SsoGrantPermissionViewModel: Calling Package is null")
        emitPermissionDeniedEvent(Constants.EXCEPTION_ACCOUNT_ACCESS_DECLINED)
        return null
    }

    private suspend fun validate(packageName: String?, account: Account?) {
        if (!isValidRequest(packageName, account)) {
            log.log(Level.SEVERE, "SsoGrantPermissionViewModel: Invalid request")
            emitPermissionDeniedEvent(Constants.EXCEPTION_ACCOUNT_ACCESS_DECLINED)
        }

        val serverUrl = getServerUrl(account!!) ?: return

        val token = UUID.randomUUID().toString().replace("-".toRegex(), "")
        val userId = getUserId(account)

        saveToken(
            token = token,
            accountName = account.name,
            packageName = packageName!!
        )

        passSuccessfulData(
            account = account,
            token = token,
            userId = userId,
            serverUrl = serverUrl
        )

    }

    private fun isValidRequest(packageName: String?, account: Account?): Boolean {
        if (packageName == null || account == null) {
            return false
        }

        return acceptedPackages.contains(packageName) && acceptedAccountTypes.contains(account.type)
    }

    private suspend fun getServerUrl(account: Account): String? {
        try {
            val ocAccount = OwnCloudAccount(account, context)
            return ocAccount.baseUri.toString()
        } catch (e: AccountUtils.AccountNotFoundException) {
            log.log(Level.SEVERE, "SsoGrantPermissionViewModel: Account not found")
            emitPermissionDeniedEvent(Constants.EXCEPTION_ACCOUNT_NOT_FOUND)
        }

        return null
    }

    private fun getUserId(account: Account): String {
        val accountManager = AccountManager.get(context)
        val userId = accountManager.getUserData(account, AccountUtils.Constants.KEY_USER_ID)

        if (!userId.isNullOrBlank()) {
            return userId
        }

        val principalUrl =
            database.serviceDao().getByAccountName(account.name)?.principal?.toString()
                ?: return account.name

        return UserIdFetcher.fetch(principalUrl) ?: account.name
    }

    private fun saveToken(token: String, accountName: String, packageName: String) {
        val hashedTokenWithSalt = EncryptionUtils.generateSHA512(token)
        val sharedPreferences =
            context.getSharedPreferences(Constants.SSO_SHARED_PREFERENCE, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(packageName + Constants.DELIMITER + accountName, hashedTokenWithSalt)
        editor.apply()
    }

    private suspend fun passSuccessfulData(
        account: Account,
        token: String,
        userId: String,
        serverUrl: String
    ) {
        val result = Bundle().apply {
            putString(AccountManager.KEY_ACCOUNT_NAME, account.name)
            putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
            putString(AccountManager.KEY_AUTHTOKEN, Constants.NEXTCLOUD_SSO)
            putString(Constants.SSO_USER_ID, userId)
            putString(Constants.SSO_TOKEN, token)
            putString(Constants.SSO_SERVER_URL, serverUrl)
        }

        _permissionEvent.emit(
            SsoGrantPermissionEvent.PermissionGranted(
                bundle = result
            )
        )
    }
}
