/*
 * Copyright MURENA SAS 2022, 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package at.bitfire.davdroid.authorization

import android.content.Context
import android.net.Uri
import at.bitfire.davdroid.BuildConfig
import at.bitfire.davdroid.R
import net.openid.appauth.AuthorizationServiceConfiguration
import net.openid.appauth.AuthorizationServiceConfiguration.RetrieveConfigurationCallback

/**
 * An abstraction of identity providers, containing all necessary info for the demo app.
 */
enum class IdentityProvider(
    discoveryEndpoint: String?,
    authEndpoint: String?,
    tokenEndpoint: String?,
    clientId: String,
    clientSecret: String?,
    redirectUri: String,
    logoutRedirectUri: String?,
    scope: String,
    userInfoEndpoint: String?,
    baseUrl: String?
) {
    MURENA(
        discoveryEndpoint = BuildConfig.MURENA_DISCOVERY_END_POINT,
        authEndpoint = null,
        tokenEndpoint = null,
        clientId = BuildConfig.MURENA_CLIENT_ID,
        clientSecret = BuildConfig.MURENA_CLIENT_SECRET,
        redirectUri = BuildConfig.MURENA_REDIRECT_URI + ":/redirect",
        logoutRedirectUri = BuildConfig.MURENA_LOGOUT_REDIRECT_URI + ":/redirect",
        scope = "openid profile email offline_access",
        userInfoEndpoint = null,
        baseUrl = BuildConfig.MURENA_BASE_URL,
    ),
    GOOGLE(
        discoveryEndpoint = "https://accounts.google.com/.well-known/openid-configuration",
        authEndpoint = null,
        tokenEndpoint = null,
        clientId = BuildConfig.GOOGLE_CLIENT_ID,
        clientSecret = null,
        redirectUri = BuildConfig.GOOGLE_REDIRECT_URI + ":/oauth2redirect",
        logoutRedirectUri = null,
        scope = "openid profile email https://www.googleapis.com/auth/carddav https://www.googleapis.com/auth/calendar https://mail.google.com/",
        userInfoEndpoint = null,
        baseUrl = null
    ),
    YAHOO(
        discoveryEndpoint = "https://api.login.yahoo.com/.well-known/openid-configuration",
        authEndpoint = null,
        tokenEndpoint = null,
        clientId = BuildConfig.YAHOO_CLIENT_ID,
        clientSecret = null,
        redirectUri = BuildConfig.APPLICATION_ID + "://oauth2redirect",
        logoutRedirectUri = null,
        scope = "openid openid2 profile email mail-w sdct-w ycal-w",
        userInfoEndpoint = null,
        baseUrl = null
    );

    companion object {
        fun retrieveByAccountType(context: Context, accountType: String): IdentityProvider? {
            return when(accountType) {
                context.getString(R.string.eelo_account_type) -> MURENA
                context.getString(R.string.google_account_type) -> GOOGLE
                context.getString(R.string.yahoo_account_type) -> YAHOO
                else -> null
            }
        }
    }

    private val mDiscoveryEndpoint: Uri?
    private val mAuthEndpoint: Uri?
    private val mTokenEndpoint: Uri?

    val clientId: String
    val clientSecret: String?
    val redirectUri: Uri
    val logoutRedirectUri: Uri?
    val scope: String
    val userInfoEndpoint: String?
    val baseUrl: String?

    init {
        require(
            !(discoveryEndpoint == null &&
                    (authEndpoint == null || tokenEndpoint == null))
        ) { "the discovery endpoint or the auth and token endpoints must be specified" }

        mDiscoveryEndpoint = retrieveUri(discoveryEndpoint)
        mAuthEndpoint = retrieveUri(authEndpoint)
        mTokenEndpoint = retrieveUri(tokenEndpoint)
        this.clientId = clientId
        this.clientSecret = clientSecret
        this.redirectUri =
            retrieveUri(redirectUri) ?: throw IllegalArgumentException("invalid redirect uri")
        this.logoutRedirectUri = retrieveUri(logoutRedirectUri)
        this.scope = scope
        this.userInfoEndpoint = userInfoEndpoint
        this.baseUrl = baseUrl
    }

    fun retrieveConfig(callback: RetrieveConfigurationCallback) {
        if (mDiscoveryEndpoint != null) {
            AuthorizationServiceConfiguration.fetchFromUrl(mDiscoveryEndpoint, callback)
        } else {
            val config = AuthorizationServiceConfiguration(mAuthEndpoint!!, mTokenEndpoint!!, null)
            callback.onFetchConfigurationCompleted(config, null)
        }
    }

    private fun retrieveUri(value: String?): Uri? {
        return if (value == null) {
            null
        } else Uri.parse(value)
    }
}
