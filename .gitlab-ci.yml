image: "registry.gitlab.e.foundation/e/os/docker-android-apps-cicd:master"

variables:
  PROJECT_ID: "83" # under Settings -> General
  APK_PATH: "app/build/outputs/apk/ose/release"
  UNSIGNED_APK: "davx5-ose-release-unsigned.apk"
  COMMUNITY_APK: "Account_manager_community.apk"
  OFFICIAL_APK: "Account_manager_official.apk"
  TEST_APK: "Account_manager_test.apk"

stages:
  - update-from-upstream
  - build
  - gitlab_release

before_script:
  - echo email.key=$PEPPER >> local.properties
  - echo MURENA_CLIENT_ID=$MURENA_CLIENT_ID >> local.properties
  - echo MURENA_CLIENT_SECRET=$MURENA_CLIENT_SECRET >> local.properties
  - echo MURENA_REDIRECT_URI=$MURENA_REDIRECT_URI >> local.properties
  - echo MURENA_LOGOUT_REDIRECT_URI=$MURENA_LOGOUT_REDIRECT_URI >> local.properties
  - echo MURENA_BASE_URL=$MURENA_BASE_URL >> local.properties
  - echo MURENA_DISCOVERY_END_POINT=$MURENA_DISCOVERY_END_POINT >> local.properties
  - echo GOOGLE_CLIENT_ID=$GOOGLE_CLIENT_ID >> local.properties
  - echo GOOGLE_REDIRECT_URI=$GOOGLE_REDIRECT_URI >> local.properties
  - echo YAHOO_CLIENT_ID=$YAHOO_CLIENT_ID >> local.properties
  - export GRADLE_USER_HOME=$(pwd)/.gradle
  - chmod +x ./gradlew

cache:
  key: ${CI_PROJECT_ID}
  paths:
    - .gradle/

build:
  stage: build
  script:
    - ./gradlew build -x test
    - cd app/build/outputs/apk/ose/
    - |
      if [[ ! -d "release" ]]; then
        echo "$APK_PATH does not exist."
        exit 1
      fi
      cd "release"
      unsigned_build=$(ls *.apk | grep "unsigned")
      cp $unsigned_build $UNSIGNED_APK
  artifacts:
    paths:
      - app/build/outputs/apk/ose/

init_submodules:
  stage: gitlab_release
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: on_success
  script:
    - git clone https://gitlab.e.foundation/e/os/system-apps-update-info.git systemAppsUpdateInfo
  artifacts:
    paths:
      - systemAppsUpdateInfo/scripts/

generate-apks:
  stage: gitlab_release
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: on_success
  needs:
    - init_submodules
    - build
  dependencies:
    - init_submodules
    - build
  before_script:
    - apt update && apt install apksigner -y
  script:
    - |
      ./systemAppsUpdateInfo/scripts/generate-apks.sh \
      "$APK_PATH" "$UNSIGNED_APK" "$COMMUNITY_APK" "$OFFICIAL_APK" "$TEST_APK"
  artifacts:
    paths:
      - $APK_PATH/$UNSIGNED_APK
      - $APK_PATH/$COMMUNITY_APK
      - $APK_PATH/$OFFICIAL_APK
      - $APK_PATH/$TEST_APK

create-json-files:
  stage: gitlab_release
  dependencies:
    - init_submodules
    - generate-apks
  needs:
    - init_submodules
    - generate-apks
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: on_success
  before_script:
    - apt update && apt install jq aapt -y
  script:
    - |
      ./systemAppsUpdateInfo/scripts/create-json-files.sh \
      "$APK_PATH" "$UNSIGNED_APK" "$COMMUNITY_APK" "$OFFICIAL_APK" "$TEST_APK"
  artifacts:
    paths:
      - community.json
      - official.json
      - test.json

create-test-release:
  stage: gitlab_release
  dependencies:
    - init_submodules
  needs:
    - init_submodules
    - create-json-files
    - generate-apks
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: manual
  before_script:
    - apt update && apt install jq -y
  script:
    - |
      ./systemAppsUpdateInfo/scripts/create-test-release.sh \
      "$APK_PATH" "$TEST_APK"
  allow_failure: true

create-release:
  stage: gitlab_release
  dependencies:
    - init_submodules
  needs:
    - init_submodules
    - create-json-files
    - generate-apks
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: manual
  before_script:
    - apt update && apt install jq -y
  script:
    - |
      ./systemAppsUpdateInfo/scripts/create-release.sh \
      "$APK_PATH" "$UNSIGNED_APK" "$COMMUNITY_APK" "$OFFICIAL_APK"
  allow_failure: true

.update-from-upstream:
  stage: update-from-upstream
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_REF_NAME == $LOCAL_BRANCH'
  variables:
    CI_PROJECT_SSH_URL: git@gitlab.e.foundation:$CI_PROJECT_PATH
    GIT_STRATEGY: none
  before_script:
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "${SSH_E_ROBOT_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "${SSH_KNOWN_HOSTS}" > ~/.ssh/known_hosts
    - echo "${SSH_KNOWN_HOSTS}"
    - chmod 644 ~/.ssh/known_hosts
    - git config --global user.email $GITLAB_USER_EMAIL
    - git config --global user.name "$GITLAB_USER_NAME"
    - cd $CI_BUILD_DIR
    - rm -rf $CI_PROJECT_DIR
    - git clone $CI_PROJECT_SSH_URL $CI_PROJECT_DIR
    - cd $CI_PROJECT_DIR
  script:
    - git config --global http.sslverify false
    # update $UPSTREAM_BRANCH & tags
    - git fetch origin
    - git checkout $UPSTREAM_BRANCH
    - git remote add upstream $UPSTREAM_URL
    - git fetch upstream
    - git pull upstream $UPSTREAM_DEFAULT_BRANCH
    - git push origin $UPSTREAM_BRANCH
    - git push origin --tags
    # checkout to latest tag commit to $TEMP_LATEST_TAG_BRANCH
    - git checkout $(git describe --tags --abbrev=0)
    - git checkout -b $TEMP_LATEST_TAG_BRANCH
    # merge $LOCAL_BRANCH with $TEMP_LATEST_TAG_BRANCH & push
    - git checkout $LOCAL_BRANCH
    - git merge $TEMP_LATEST_TAG_BRANCH
    - git push origin $LOCAL_BRANCH
    # remove unwanted local branch & remote
    - git branch -D $TEMP_LATEST_TAG_BRANCH
    - git remote remove upstream

update-default-branch:
  extends: .update-from-upstream
  variables:
    LOCAL_BRANCH: main
    UPSTREAM_BRANCH: upstream/master
    UPSTREAM_DEFAULT_BRANCH: release-ose
    UPSTREAM_URL: https://github.com/bitfireAT/davx5-ose.git
    TEMP_LATEST_TAG_BRANCH: latest_upstream_tag_branch


