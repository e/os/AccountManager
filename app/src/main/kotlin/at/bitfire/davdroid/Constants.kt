/***************************************************************************************************
 * Copyright © All Contributors. See LICENSE and AUTHORS in the root directory for details.
 **************************************************************************************************/
package at.bitfire.davdroid

object Constants {

    const val DAVDROID_GREEN_RGBA = 0xFF8bc34a.toInt()

    // gplay billing
    const val BILLINGCLIENT_CONNECTION_MAX_RETRIES = 4

    // NOTE: Android 7 and up don't allow 2 min sync frequencies unless system frameworks are modified
    const val DEFAULT_CALENDAR_SYNC_INTERVAL = 15 * 60L // 15 minutes
    const val DEFAULT_CONTACTS_SYNC_INTERVAL = 15 * 60L // 15 minutes

    /**
     * Context label for [org.apache.commons.lang3.exception.ContextedException].
     * Context value is the [at.bitfire.davdroid.resource.LocalResource]
     * which is related to the exception cause.
     */
    const val EXCEPTION_CONTEXT_LOCAL_RESOURCE = "localResource"

    /**
     * Context label for [org.apache.commons.lang3.exception.ContextedException].
     * Context value is the [okhttp3.HttpUrl] of the remote resource
     * which is related to the exception cause.
     */
    const val EXCEPTION_CONTEXT_REMOTE_RESOURCE = "remoteResource"

    const val AUTH_TOKEN_TYPE = "oauth2-access-token"

    const val EELO_SYNC_HOST = "murena.io"
    const val E_SYNC_URL = "e.email"

    const val MURENA_DAV_URL = "https://murena.io/remote.php/dav"

    const val HTTP_STATUS_CODE_TOO_MANY_REQUESTS = 429
}
