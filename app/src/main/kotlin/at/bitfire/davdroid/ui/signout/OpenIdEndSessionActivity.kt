/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.ui.signout

import android.accounts.AccountManager
import android.app.Activity
import android.os.Bundle
import at.bitfire.davdroid.authorization.IdentityProvider
import at.bitfire.davdroid.settings.AccountSettings
import net.openid.appauth.AuthState
import net.openid.appauth.AuthorizationService
import net.openid.appauth.AuthorizationServiceConfiguration
import net.openid.appauth.EndSessionRequest

class OpenIdEndSessionActivity : Activity() {

    private var authorizationService: AuthorizationService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val authStateString = intent.getStringExtra(AccountSettings.KEY_AUTH_STATE)
        val accountType = intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE)

        if (authStateString.isNullOrBlank() || accountType.isNullOrBlank()) {
            finish()
            return
        }

        val authState = AuthState.jsonDeserialize(authStateString)

        val configuration = authState.authorizationServiceConfiguration
        if (configuration?.endSessionEndpoint == null) {
            finish()
            return
        }

        startEndSessionWebIntent(
            accountType = accountType,
            configuration = configuration,
            authState = authState
        )

        finish()
    }

    private fun startEndSessionWebIntent(
        accountType: String,
        configuration: AuthorizationServiceConfiguration,
        authState: AuthState
    ) {
        authorizationService = AuthorizationService(applicationContext)

        val redirectUri =
            IdentityProvider.retrieveByAccountType(this, accountType)?.logoutRedirectUri

        val intent = authorizationService!!.getEndSessionRequestIntent(
            EndSessionRequest.Builder(configuration)
                .setIdTokenHint(authState.idToken)
                .setPostLogoutRedirectUri(redirectUri)
                .build()
        )

        startActivity(intent)
    }

    override fun onDestroy() {
        authorizationService?.dispose()
        super.onDestroy()
    }
}
