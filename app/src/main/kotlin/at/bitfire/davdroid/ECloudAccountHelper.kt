/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid

import android.accounts.AccountManager
import android.app.Activity
import android.content.Context
import com.google.android.material.dialog.MaterialAlertDialogBuilder

object ECloudAccountHelper {

    fun alreadyHasECloudAccount(context: Context) : Boolean {
        val accountManager = AccountManager.get(context)
        val eCloudAccounts = accountManager.getAccountsByType(context.getString(R.string.eelo_account_type))
        return eCloudAccounts.isNotEmpty()
    }

    fun showMultipleECloudAccountNotAcceptedDialog(activity: Activity) {
        MaterialAlertDialogBuilder(activity, R.style.CustomAlertDialogStyle)
                .setIcon(R.drawable.ic_error)
                .setMessage(R.string.multiple_ecloud_account_not_permitted_message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    activity.finish()
                }
                .show()
    }
}