/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.nextcloud.android.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import at.bitfire.davdroid.Constants;
import at.bitfire.davdroid.R;
import at.bitfire.davdroid.settings.AccountSettings;

public final class AccountManagerUtils {

    private AccountManagerUtils() {
        // utility class -> private constructor
    }

    @Nullable
    public static Account getAccountByName(@NonNull Context context, String name) {
        for (Account account : getAccounts(context)) {
            if (account.name.equals(name)) {
                return account;
            }
        }

        return null;
    }

    @Nullable
    public static boolean isOidcAccount(@NonNull Context context, @NonNull Account account) {
        AccountManager accountManager = AccountManager.get(context);
        String authState = accountManager.getUserData(account, AccountSettings.KEY_AUTH_STATE);
        return authState != null && !authState.trim().isEmpty();
    }

    @NonNull
    public static Account[] getAccounts(@NonNull Context context) {
        AccountManager accountManager = AccountManager.get(context);
        return accountManager.getAccountsByType(getAccountType(context));
    }

    @NonNull
    public static String getAccountType(@NonNull Context context) {
        return context.getString(R.string.eelo_account_type);
    }

}
