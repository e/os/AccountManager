/***************************************************************************************************
 * Copyright © All Contributors. See LICENSE and AUTHORS in the root directory for details.
 **************************************************************************************************/

package at.bitfire.davdroid.db

import net.openid.appauth.AuthState
import java.net.URI

data class Credentials(
    val userName: String? = null,
    val password: String? = null,
    val authState: AuthState? = null,
    val certificateAlias: String? = null,
    val serverUri: URI? = null,
    val clientSecret: String? = null,
    var passwordNeedsUpdate: Boolean = false
) {

    override fun toString(): String {
        val s = mutableListOf<String>()

        if (userName != null)
            s += "userName=$userName"
        if (password != null)
            s += "password=******"

        if (certificateAlias != null)
            s += "certificateAlias=$certificateAlias"

        if (authState != null)
            s += "authState=******"

        if (clientSecret != null)
            s += "clientSecret=******"

        return "Credentials(" + s.joinToString(", ") + ")"
    }

}
