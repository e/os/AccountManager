/***************************************************************************************************
 * Copyright © All Contributors. See LICENSE and AUTHORS in the root directory for details.
 **************************************************************************************************/

package at.bitfire.davdroid.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.CallSuper
import at.bitfire.davdroid.R

/**
 * Default menu items control
 */
abstract class BaseAccountsDrawerHandler: AccountsDrawerHandler {

    @CallSuper
    override fun initMenu(context: Context, menu: Menu) {
        // TODO Provide option for beta feedback
    }

    @CallSuper
    override fun onNavigationItemSelected(activity: Activity, item: MenuItem) {
        when (item.itemId) {
            R.id.nav_about ->
                activity.startActivity(Intent(activity, AboutActivity::class.java))
            R.id.nav_privacy ->
                activity.startActivity(Intent(activity, PrivacyPolicyActivity::class.java))
            R.id.nav_app_settings ->
                activity.startActivity(Intent(activity, AppSettingsActivity::class.java))
        }
    }

}