/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.ui.setup

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import at.bitfire.davdroid.R
import at.bitfire.davdroid.authorization.IdentityProvider
import at.bitfire.davdroid.db.Credentials
import at.bitfire.davdroid.log.Logger
import at.bitfire.davdroid.ui.NetworkUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.openid.appauth.AuthState.AuthStateAction
import net.openid.appauth.AuthorizationException
import net.openid.appauth.AuthorizationResponse
import net.openid.appauth.AuthorizationService.TokenResponseCallback
import net.openid.appauth.AuthorizationServiceConfiguration
import org.json.JSONObject
import java.net.URI
import java.util.logging.Level

abstract class OpenIdAuthenticationBaseFragment(private val identityProvider: IdentityProvider) :
    Fragment() {

    private val viewModel by viewModels<OpenIdAuthenticationViewModel>()
    private val loginModel by activityViewModels<LoginModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.identityProvider = identityProvider

        return inflater.inflate(R.layout.frament_openid_auth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (isAuthFlowComplete()) {
            val setupState = viewModel.setUpAuthState(requireActivity().intent)
            if (setupState == OpenIdAuthStateSetupState.ALREADY_SET_UP) {
                return
            }

            if (setupState == OpenIdAuthStateSetupState.SET_UP_FAILED) {
                handleLoginFailedToast()
                return
            }

            extractAuthCodeResponse()
        }
    }

    private fun extractAuthCodeResponse() {
        val response = AuthorizationResponse.fromIntent(requireActivity().intent)
        val exception = AuthorizationException.fromIntent(requireActivity().intent)

        if (response == null || exception != null) {
            Logger.log.log(Level.SEVERE, "Failed to retrieve auth response", exception)
            handleLoginFailedToast()
            return
        }

        performTokenRequest(response, exception)
    }

    protected fun isAuthFlowComplete(): Boolean {
        return activity?.intent?.getBooleanExtra(
            LoginActivity.OPENID_AUTH_FLOW_COMPLETE,
            false
        ) == true
    }

    protected fun startAuthFLow() {
        if (isAuthFlowComplete()) {
            return
        }

        if (handleNoNetworkToast()) {
            return
        }

        viewModel.identityProvider?.retrieveConfig { serviceConfiguration, exception ->
            if (exception != null || serviceConfiguration == null) {
                Logger.log.log(Level.SEVERE, "failed to fetch configuration", exception)
                handleLoginFailedToast()
                return@retrieveConfig
            }

            obtainAuthCode(serviceConfiguration)
        }
    }

    private fun obtainAuthCode(serviceConfiguration: AuthorizationServiceConfiguration) {
        if (handleNoNetworkToast()) {
            return
        }

        viewModel.requestAuthCode(serviceConfiguration, requireActivity().intent)
        requireActivity().setResult(Activity.RESULT_OK)
        requireActivity().finish()
    }

    private fun performTokenRequest(
        authorizationResponse: AuthorizationResponse,
        authorizationException: AuthorizationException?
    ) {
        if (handleNoNetworkToast()) {
            return
        }

        viewModel.performTokenRequest(
            authorizationResponse,
            authorizationException,
            getTokenResponseCallback()
        )
    }

    private fun getTokenResponseCallback(): TokenResponseCallback {
        return TokenResponseCallback { response, exception ->
            if (response == null || exception != null) {
                Logger.log.log(Level.SEVERE, "failed to retrieve token", exception)
                handleLoginFailedToast()
                return@TokenResponseCallback
            }

            viewModel.retrieveAccountInfo(response, exception, getAuthStateActionCallback())
        }
    }

    private fun getAuthStateActionCallback(): AuthStateAction {
        return AuthStateAction { accessToken, _, exception ->
            if (exception != null || accessToken == null) {
                Logger.log.log(Level.SEVERE, "failed to retrieve user info", exception)
                handleLoginFailedToast()
                return@AuthStateAction
            }

            val infoEndpoint = viewModel.getUserInfoEndpoint()
            if (infoEndpoint == null) {
                handleLoginFailedToast()
                return@AuthStateAction
            }

            lifecycleScope.launch(Dispatchers.IO) {
                val infoJson = viewModel.retrieveUserInfoFromServer(infoEndpoint, accessToken)
                if (infoJson == null) {
                    lifecycleScope.launch(Dispatchers.Main) {
                        handleLoginFailedToast()
                    }
                    return@launch
                }

                lifecycleScope.launch(Dispatchers.Main) {
                    onAuthenticationComplete(infoJson)
                }
            }
        }
    }

    private fun finishActivity() {
        this@OpenIdAuthenticationBaseFragment.requireActivity().finish()
    }

    private fun handleNoNetworkToast(): Boolean {
        if (NetworkUtils.isConnectedToNetwork(requireContext())) {
            return false
        }

        Toast.makeText(requireContext(), R.string.no_internet_toast, Toast.LENGTH_LONG).show()
        finishActivity()
        return true
    }

    protected fun handleLoginFailedToast() {
        Toast.makeText(requireContext(), R.string.login_failed, Toast.LENGTH_LONG).show()
        finishActivity()
    }

    protected fun proceedNext(userName: String, baseUrl: String, cardDavUrl: String? = null) {
        activity?.intent?.putExtra(LoginActivity.OPENID_AUTH_FLOW_COMPLETE, true)

        if (cardDavUrl != null) {
            loginModel.cardDavURI = URI(cardDavUrl)
        }

        val baseUri = URI(baseUrl)
        loginModel.baseURI = baseUri
        loginModel.credentials = Credentials(
            userName,
            null,
            viewModel.getAuthState(),
            null,
            baseUri,
            identityProvider.clientSecret
        )

        parentFragmentManager.beginTransaction()
            .replace(android.R.id.content, DetectConfigurationFragment(), null)
            .addToBackStack(null)
            .commit()
    }

    protected abstract fun onAuthenticationComplete(userData: JSONObject)
}
