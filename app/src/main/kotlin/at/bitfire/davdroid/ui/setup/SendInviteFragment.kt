/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package at.bitfire.davdroid.ui.setup

import android.accounts.AccountManager
import android.accounts.AccountManagerCallback
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import at.bitfire.davdroid.BuildConfig
import at.bitfire.davdroid.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import okhttp3.*
import java.io.IOException
import java.math.BigInteger
import java.security.MessageDigest

class SendInviteFragment : Fragment() {

    private var email = ""
    private val key = BuildConfig.EMAIL_KEY

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_send_invite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val emailTextField = view.findViewById<EditText>(R.id.emailId)
        val emailTextLayout = view.findViewById<TextInputLayout>(R.id.emailId_layout)
        val sendButton = view.findViewById<Button>(R.id.send_button)
        emailTextField.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(p0.toString().trim().isNotEmpty()) {
                    sendButton.isEnabled = true
                    sendButton.setBackgroundColor(resources.getColor(R.color.create_account_input_enabled_color))
                    emailTextLayout.boxStrokeColor = resources.getColor(R.color.create_account_input_enabled_color)
                    emailTextLayout.hintTextColor = resources.getColorStateList(R.color.create_account_input_enabled_color)
                } else {
                    sendButton.isEnabled = false
                    sendButton.setBackgroundColor(resources.getColor(R.color.create_account_input_disabled_color))
                    emailTextLayout.boxStrokeColor = resources.getColor(R.color.create_account_input_disabled_color)
                    emailTextLayout.hintTextColor = resources.getColorStateList(R.color.create_account_input_disabled_color)
                }
                emailTextLayout.isErrorEnabled = false
            }
            override fun afterTextChanged(p0: Editable?) {

            }
        })

        sendButton.setOnClickListener {
            email = emailTextField.text.toString()
            if(isNetworkAvailable()) {
                createAccount()
            } else {
                Toast.makeText(context, R.string.no_internet_toast, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun createAccount() {
        val client = OkHttpClient()
        val userAgentKey = "User-Agent"
        val userAgentValue = "Java 11 HttpClient Bot"
        val contentKey = "Content-Type"
        val contentValue = "application/x-www-form-urlencoded"
        val inviteUrl = "https://murena.io/signup/process_email_invite.php"

        val formBody = FormBody.Builder()
            .add("email", email)
            .add("check", secretCode())
            .build()

        val request = Request.Builder()
            .post(formBody)
            .header(userAgentKey, userAgentValue)
            .addHeader(contentKey, contentValue)
            .url(inviteUrl)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) throw IOException("Unexpected code $response")
                val stringCode = response.body?.string()
                activity?.runOnUiThread {
                    stringCode?.let { getIntCode(it) }?.let { handleErrorCode(it) }
                }

            }
        })
    }

    private fun secretCode(): String {
        val md: MessageDigest = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest((email + key).toByteArray())).toString(16).padStart(32, '0')
    }

    private fun getIntCode(stringCode: String): Int {
        val trimedResult: String = stringCode.trim()
        val errorCode = trimedResult.substring(
            trimedResult.lastIndexOf(":") + 1,
            trimedResult.length - 1
        )

        return try {
            errorCode.toInt()
        } catch (e: NumberFormatException) {
            -1
        }
    }

    private fun handleErrorCode(intCode: Int) {
        when(intCode) {
            100 -> {
                requireFragmentManager().beginTransaction()
                    .replace(R.id.content, InviteSuccessfulFragment())
                    .commit()
            }
            200 -> {
                activity?.let {
                    MaterialAlertDialogBuilder(it, R.style.CustomAlertDialogStyle)
                        .setIcon(R.drawable.ic_error)
                        .setTitle(R.string.user_exists_dialog_title)
                        .setMessage(R.string.user_exists_dialog_message)
                        .setPositiveButton(R.string.skip_button) { _,_ ->
                            activity?.setResult(Activity.RESULT_OK, Intent())
                            activity?.finish()
                        }
                        .setNegativeButton(R.string.login_button) { _,_ ->
                            try {
                                val accountManager = AccountManager.get(it)
                                val accountType = it.getString(R.string.eelo_account_type)
                                accountManager.addAccount(
                                    accountType,
                                    null,
                                    null,
                                    null,
                                   it,
                                    AccountManagerCallback<Bundle?> {
                                        activity?.setResult(Activity.RESULT_OK, Intent())
                                        activity?.finish()
                                    },
                                    null
                                )
                            } catch (e: Exception) {
                            }
                        }
                        .show()
                }
            }
            300 -> {
                activity?.let {
                    val emailField = it.findViewById<TextInputLayout>(R.id.emailId_layout)
                    emailField.isErrorEnabled = true
                    emailField.error = getString(R.string.emailid_error)
                }

            }
            else -> {
                activity?.let {
                    MaterialAlertDialogBuilder(it, R.style.CustomAlertDialogStyle)
                        .setIcon(R.drawable.ic_error)
                        .setTitle(R.string.error_dialog_title)
                        .setMessage(R.string.error_dialog_message)
                        .setPositiveButton(R.string.skip_button) { _,_ ->
                            activity?.setResult(Activity.RESULT_OK, Intent())
                            activity?.finish()
                        }
                        .setNegativeButton(R.string.try_again_button) { _,_ ->
                            createAccount()
                        }
                        .show()
                }
            }
        }
    }
    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

}
