package at.bitfire.davdroid.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import com.nextcloud.android.utils.WebViewUtils

// URL constant
const val PRIVACY_POLICY_URL = "https://e.foundation/legal-notice-privacy/#account-manager"

class PrivacyPolicyActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WebViewUtils.openCustomTab(this, PRIVACY_POLICY_URL)
        finishAfterTransition() // Finish the activity after launching the custom tab
    }
}

